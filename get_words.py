import  requests
from lxml import etree
from lxml import html


def get_word_one():
    url = "http://service.alibaba.com/supplier/faq_detail/13858245.htm"
    list = []
    try:
        r = requests.get(url)
        r.encoding =  r.apparent_encoding
        page = etree.HTML(r.text)
    except requests.exceptions.HTTPError as e:
        pass
    finally:
        r.close()
    xpath = "//*[@class=\"mceItemTable\"]/tbody/tr"
    result = page.xpath(xpath)
    # result1 = etree.HTML(result[2])
    #for item in result:
    '''
    str = etree.tostring(result[4])
    print str
    tr_item =  etree.HTML(str)
    xpath1 = "//td"
    result1 = tr_item.xpath(xpath1)
    print result1[0].
    '''
    i = 0
    for tr_item in result:
        i = i + 1
        if i==1 or i==2:
            continue
        key = ""
        value = ""
        for ele in tr_item.iter():
            if ele.tag == "td":
                if ele.text is None:
                    tr_pword = etree.tostring(ele)
                    tr_pword_txt = html.fromstring(tr_pword)
                    pword = tr_pword_txt.text_content()
                    if pword != "":
                        key = pword
                else:
                        value = ele.text
        list.append({key : value})
    return list

def get_word_two():
    url = "http://service.alibaba.com/supplier/faq_detail/13858244.htm"
    list = []
    try:
        r = requests.get(url)
        r.encoding =  r.apparent_encoding
        page = etree.HTML(r.text)
    except requests.exceptions.HTTPError as e:
        pass
    finally:
        r.close()
    xpath = "//*[@class=\"aliDataTable mceItemTable\"]/tbody/tr"
    result = page.xpath(xpath)
    # result1 = etree.HTML(result[2])
    #for item in result:
    '''
    str = etree.tostring(result[4])
    print str
    tr_item =  etree.HTML(str)
    xpath1 = "//td"
    result1 = tr_item.xpath(xpath1)
    print result1[0].
    '''
    for tr_item in result:
        str = ""
        pw_key = ""
        pw_value = ""
        td_index = 0
        td_count = 0
        for ele in tr_item.iter():
            if ele.tag == "td":
                tr_pword = etree.tostring(ele)
                tr_pword_txt = html.fromstring(tr_pword)
                pword = tr_pword_txt.text_content()
                if pword != "":
                    str = str + " " + pword
                    if td_index == 0:
                        pw_value = pword.strip()
                    if td_index == 1:
                        pw_key = pword.strip()
                        if pword == "":
                           print etree.tostring(ele)
                    td_index = td_index + 1
        list.append({pw_key : pw_value})
    return list



if __name__ == '__main__':
    list = get_word_two()
    '''
    for dict in list:
        for (d,x) in dict.items():
            if ',' in d or '(' in d:
                print x+" "+d
    '''
    print len(list)
