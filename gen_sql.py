# coding: utf-8
from get_words import get_word_one
from get_words import get_word_two
from get_industry import get_db_industry_id
import datetime

def gen_not_exists_sql():
    list1 = get_word_one()
    list2 = get_word_two()
    list_collection = list1 + list2

    list1.extend(list2)
    list_db = []
    # 正常sql语句
    list_sql_normal = []
    # 找不到Category
    list_sql_error = []
    # 含有逗号 括号
    list_sql_split = []




    # 读取数据库违禁词
    with open("1.cvs") as f:
        for line in f:
            list_db.append(line.strip())

    for dict in list_collection:
        for (key,value) in dict.items():
            if key.strip() == "":
                continue
            if not key in list_db:
                industry_index = get_db_industry_id(value)
                now_str = datetime.datetime.now().strftime('%Y-%m-%d')
                if "'" in key:
                    key = key.replace("'", "''")
                sql_base = "INSERT INTO GA_PWord_ProhibitedWordPublic VALUES ('%s', '%s', '1', '%s');" % (key.strip(), industry_index, now_str)
                if industry_index == "-1":
                    list_sql_error.append(sql_base)
                else:
                    if ',' in key or '(' in key:
                        list_sql_split.append(sql_base)
                    else:
                        list_sql_normal.append(sql_base)


    '''
    f_sql = file("1.sql", "w+")
    for line in list_write:
        try:
            f_sql.writelines(line)
        except TypeError:
            print line
    f_sql.close()
    '''
    print "----------- normal-------------"
    for em in list_sql_normal:
        print em
    print "----------- spite-------------"
    for em in list_sql_split:
        print em
    print "----------- error-------------"
    for em in list_sql_error:
        print em
    # print list_sql

if __name__ == '__main__':
    gen_not_exists_sql()
