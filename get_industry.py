# -*- coding: utf-8 -*-
import  requests
from lxml import etree
from lxml import html

def get_db_industry():
    industry_db_dict = {
        "1":u"包装及材料",
        "2":u"服饰箱包",
        "3":u"高精尖 (3C产品)",
        "4":u"护理产品",
        "5":u"家电",
        "6":u"家具家居",
        "7":u"交通工具",
        "8":u"礼品玩具",
        "9":u"美容美发",
        "10":u"建筑类",
        "11":u"器械工具",
        "12":u"日用化学品",
        "13":u"食品烟酒",
        "14":u"医药产品",
        "15":u"音像制品",
        "16":u"运动及娱乐产品",
        "17":u"其他",
        "19":u"电子烟行业",
        "20":u"美容健康"
    }
    return industry_db_dict

def get_db_industry_id(industry_name):
    dict = get_db_industry()
    for k,v in dict.iteritems():
        if v in industry_name:
            return k
    return "-1"


def get_industry():
    url = "http://service.alibaba.com/supplier/faq_detail/13858245.htm"
    list = []
    try:
        r = requests.get(url)
        r.encoding =  r.apparent_encoding
        page = etree.HTML(r.text)
    except requests.exceptions.HTTPError as e:
        pass
    finally:
        r.close()
    xpath = "//*[@class=\"table\"]/tbody/tr/td/a"
    result = page.xpath(xpath)
    for item in result:
        # dict = {item.get("href") : item.text}
        list.append(item.text)

    return list

if __name__ == '__main__':
    # print get_industry()[0]
    print get_db_industry()
